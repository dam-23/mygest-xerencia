import openpyxl
from datetime import datetime


def insertar(libro, fila, ws, total):
    fecha = datetime.now()
    if fecha.month == 1:
        ws['D' + str(fila)] = total
    elif fecha.month == 2:
        ws['E' + str(fila)] = total
    elif fecha.month == 3:
        ws['F' + str(fila)] = total
    elif fecha.month == 4:
        ws['G' + str(fila)] = total
    elif fecha.month == 5:
        ws['H' + str(fila)] = total
    elif fecha.month == 6:
        ws['I' + str(fila)] = total
    elif fecha.month == 7:
        ws['J' + str(fila)] = total
    elif fecha.month == 8:
        ws['K' + str(fila)] = total
    elif fecha.month == 9:
        ws['L' + str(fila)] = total
    elif fecha.month == 10:
        ws['M' + str(fila)] = total
    elif fecha.month == 11:
        ws['N' + str(fila)] = total
    elif fecha.month == 12:
        ws['O' + str(fila)] = total
    libro.save('Resources/informe-xerencia.xlsx')


def abrirExcel(ruta, hoja):
    wb = openpyxl.load_workbook(ruta)
    ws = wb[hoja]
    return wb, ws


def gestion_compras(hoja, fila=3, total_compras=0.0):
    for row in hoja.iter_rows(min_row=3, values_only="true"):
        celda = hoja.cell(row=fila, column=2)
        numero = celda.value
        print(numero)
        total_compras += numero
        fila += 1
        print("------------------------------------------------------------")
        print(f"El gasto total de las facturas es: {total_compras}")
        print("------------------------------------------------------------")
        return total_compras


def gestion_nominas(nominasExcel, sumaTotalNominas=0.0):
    for sheet in nominasExcel:
        if sheet.title == "NominaMuestra":
            continue
        else:
            print("------------" + sheet.title + "------------")
            salarioBase = sheet['F11']
            incentivos = sheet['F15']
            incentivosInt = incentivos.value
            pagasExtra = sheet['F16']
            if incentivosInt is None:
                incentivosInt = 0
            sumaNominas = salarioBase.value + incentivosInt + pagasExtra.value
            fecha = sheet["C8"].value
            print("El mes es: " + str(fecha.month))
            sumaTotalNominas += sumaNominas
            print("Suma del empleado: " + sheet.title + " es " + str(round(sumaNominas, 2)))
    print("------------------------------------------------------------")
    print("La suma total es: " + str(round(sumaTotalNominas, 2)))
    print("------------------------------------------------------------")
    '''cell_range = comprasHoja['B3':'B12']
            for row in cell_range:
                for cell in row:
                    sumaCompras += cell.value
                    print(cell.value)

            print(sumaCompras)'''
    return sumaTotalNominas


def save(libroGestion):
    libroGestion.save('Resources/informe-xerencia.xlsx')


def gestion_ventas(hojaVentas):
    celda = hojaVentas.cell(row=2, column=2)
    total = celda.value
    print("------------------------------------------------------------")
    print(f'El total de las ventas asciende a {total}')
    print("------------------------------------------------------------")
    return total


def main():
    if __name__ == "__main__":
        libroGestion, hojaGestion = abrirExcel('Resources/informe-xerencia.xlsx', 'informe')

        # Gestion compras
        libroCompras, hojaCompras = abrirExcel('../compras/mygest-compras/Resources/compras-procesadas.xlsx',
                                               'facturas')
        total = gestion_compras(hojaCompras)
        insertar(libroGestion, 4, hojaGestion, total)

        # Gestion Nominas
        libroNominas, hojaNominas = abrirExcel("../nominas/mygest-nominas/myGest/nominas/nominas.xlsx", "NominaMuestra")
        total = gestion_nominas(libroNominas)
        insertar(libroGestion, 3, hojaGestion, total)

        # Gestion Ventas
        libroVentas, hojaVentas = abrirExcel('../ventas/MyGuestVentas/Resources/documento_ventas.xlsx', 'Ventas')
        total = gestion_ventas(hojaVentas)
        insertar(libroGestion, 10, hojaGestion, total)


main()
