import openpyxl
import formulas

if __name__ == "__main__":

    comprasExcel = openpyxl.load_workbook("../compras/mygest-compras/Resources/compras-procesadas.xlsx")
    nominasExcel = openpyxl.load_workbook("../nominas/mygest-nominas/myGest/nominas/nominas.xlsx")
    xerenciaExcel = openpyxl.load_workbook("gestion-gestionado.xlsx")

    comprasHoja = comprasExcel.active
    xerenciaHoja = xerenciaExcel.active
    sumaTotalNominas = 0
    for sheet in nominasExcel:
        if sheet.title == "NominaMuestra":
            None
        else:
            print("------------" + sheet.title + "------------")
            salarioBase = sheet['F11']
            incentivos = sheet['F15']
            incentivosInt = incentivos.value
            pagasExtra = sheet['F16']
            if incentivosInt is None:
                incentivosInt = 0
            sumaNominas = salarioBase.value + incentivosInt + pagasExtra.value
            fecha = sheet["C8"].value
            print("El mes es: " + str(fecha.month))
            sumaTotalNominas += sumaNominas
            print("Suma del empleado: " + sheet.title + " es " + str(round(sumaNominas, 2)))
    print("------------------------------------------------------------")
    print("La suma total es: " + str(round(sumaTotalNominas, 2)))
    xerenciaHoja['A2'] = sumaTotalNominas
    xerenciaExcel.save('gestion-gestionado.xlsx')
    print("------------------------------------------------------------")

    '''cell_range = comprasHoja['B3':'B12']
    for row in cell_range:
        for cell in row:
            sumaCompras += cell.value
            print(cell.value)

    print(sumaCompras)'''
